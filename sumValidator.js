const array = [3,5,-4,8,11,1,-1,6];
const targetSum = 10;

console.log("Encontraremos dos numeros que sumen " + targetSum + " dentro de un array dado:\n");

function twoNumberSum(array,targetSum){
    arrayFinal = [];
    for (i = 0; i < array.length; i++){
        temp = array[i];
        for (j = 0; j < array.length; j++){
            if (i != j){
                suma = temp +array[j];
                //console.log("suma de :" + temp + "+" + array[j] + " es: " + suma);
                if (suma == targetSum){
                    arrayFinal = [temp,array[j]];
                    console.log("Los numeros que coinciden son: [" + arrayFinal + "]");
                    return arrayFinal;
                }
            }
            else{
                continue;
            }
     
        }
    }
    if (arrayFinal.length == 0){
        console.log("No hay coincidencias! :(");
        return arrayFinal;
    }
}

twoNumberSum(array,targetSum);